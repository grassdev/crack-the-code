const express = require('express');
const app = express();
app.use(express.static('public/'));
const port = 3000;

app.get('/', (request, response) => {
    response.sendFile(__dirname, 'index.html');
})

app.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err);
  }

  console.log(`server is listening on ${port}`);
})