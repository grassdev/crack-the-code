//Let's be classy
let someObject = {
    value: 42,
    aFunction: function(){
        return 5 + 5;
    }
}
let result = someObject.value;
//result:_____________________

result = someObject.aFunction();
//result:_____________________

//what if we wanted to change the value of value?  Wouldn't it be nice to have a function that does that?
//we could just make one:
let valueChanger = function(){
    someObject.value++;
}
valueChanger();
result = someObject.value;
//result:______________________

//well that's well and good, but it's not very organized.  It would be nice to have an object that
//can own it's own functions.  That way we can keep things tidy.  Just so happens there is!!
class Thing {
    constructor(value){
        this.value = value;
    }
    aFunction(){
        this.value++;
    }
}
let aThing = new Thing(15);
result = aThing.value;
//result:______________________

aThing.aFunction();
result = aThing.value;
//result:______________________

aThing.aFunction();
aThing.aFunction();
aThing.aFunction();
result = aThing.value;
//result:______________________

if(aThing.value === 15){
    result = aThing.value / 3;
} else if (aThing.value === 20){
    result = aThing.value - 10;
} else if (aThing.value !== 19){
    result = 19;
} else if (aThing.value >= 15){
    result = 50;
}
//result:_______________________

let anotherThing = new Thing(500);
result = anotherThing === aThing;
//result:_______________________

result = anotherThing.value === aThing.value;
//result:________________________

result = aThing.value < anotherThing.value;
//result:________________________