//Copy this to the very top of your app.js file.  This will give you a variable called "functionModule"
//any functions you setup in the "functions.js" file will be able to be used in your app.js.
import { functionsModule } from './functions.js';

//We add these buttons to the html/view to handle saying yes or no
functionsModule.addButtonWithHandlerToElement("YES", answers, yesEventHandler);
functionsModule.addButtonWithHandlerToElement("NO", answers, noEventHandler);

//We can add this to test out incrementing a variable with a function
functionsModule.addButtonWithHandlerToElement("click me a few times and watch the console", answers, functionsModule.incrementCounterToXandThenDoSomething);