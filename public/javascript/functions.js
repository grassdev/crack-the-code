//Just adds two numbers together.  Watch out, it will try to add anything, not just numbers!!
let addTwoNumbers = function(firstNumber, secondNumber){
    return firstNumber + secondNumber;
}

//Takes an outer html(the view) element and replaces it's contents with the text passed in.
let setTextOnElement = function(elementContainer, text){
    let div = document.createElement('div');
    div.setAttribute('class', 'dialogue');
    div.append(createParagraphElementWithText(text));
    clearElement(elementContainer);
    elementContainer.append(div);
}

//Takes some text and creates a paragraph element to be used with other view (html) elements.
let createParagraphElementWithText = function(theText){
    let paragraph = document.createElement('p');
    paragraph.innerText = theText;
    return paragraph;
}

//Give it a name, give it what container (part of the html/view) you want to attach it and the function you want it to call when it's clicked!
let addButtonWithHandlerToElement = function(buttonName, buttonContainer, buttonHandlerFunction){
    let button = document.createElement('button');
    button.innerText = buttonName;
    button.addEventListener('click', buttonHandlerFunction);
    buttonContainer.append(button);
}

//Helper method to clean all the contents of a container (piece of the view/html)
let clearElement = function(element){
    element.innerHTML = '';
}

//this is a variable we can use to count with
let counter = 0;

//this can be our number we check for
const howMany = 6;

//example doSomethingFunction
let doSomethingFunction = function(){
    console.log("hey, you clicked " + counter + " times!")
}

let incrementCounterToXandThenDoSomething = function(){
    counter++;
    if(counter >= howMany) {
        doSomethingFunction();
    }
}

//Don't forget to add any functions you create to this list.
const functionsModule = {
    addTwoNumbers: addTwoNumbers,
    setTextOnElement: setTextOnElement,
    createParagraphElementWithText: createParagraphElementWithText,
    addButtonWithHandlerToElement: addButtonWithHandlerToElement,
    clearElement: clearElement,
    incrementCounterToXandThenDoSomething: incrementCounterToXandThenDoSomething
};

export { functionsModule }