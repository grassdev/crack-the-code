//If we had an html file with this in it:
<div id="identity">
    Some Words
</div>

//and then we had a javascript file loaded with this in it:
let element = document.querySelector('#identity');
element.innerText = "Other Words";
//What do you think the webpage would display?
//____________________________________________________________
//you could try it in codepen.io

class Mammal {
    constructor(legs, species, name){
        this.legs = legs;
        this.species = species;
        this.name = name;
        this.hairy = true;
    }

    giveLiveBerth(){
        return new Mammal(this.legs, this.species, "Jr");
    }
}
let cow = new Mammal(4, "bovine", "Meatloaf");

let value = cow.hairy;
//value:_______________________________________________________

value = cow.name;
//value:_______________________________________________________

value = cow.legs;
//value:_______________________________________________________

value = cow.giveLiveBerth();
//value:_______________________________________________________
//_____________________________________________________________

class Cow extends Mammal {
    constructor(name){
        super(4, "bovine", name);
    }

    moo(){
        console.log("I like to moooooove it!");
    }
}
let superCow = new Cow("Steak");

value = superCow.species;
//value:_______________________________________________________

value = superCow.giveLiveBerth();
value = value === superCow;
//value:________________________________________________________

value = superCow.moo();
//value:________________________________________________________

//what if i wanted to make a herd of cows??
let herd = [];
let amount = 10;
for(let i = 0; i < amount; i++){
    herd.push(new Cow(`cow number ${i+1}`));
}

value = herd.length;
//value:_________________________________________________________

value = herd[4];
//value.name:____________________________________________________