//Can I drive yet??
let car = {
    color: "blue",
    hp: 420
}
let bike = {
    color: "green",
    hp: 15
}
function canIDrive(age){
    if(age > 16){
        return car;
    } else {
        return bike;
    }
}
let myVehicle = canIDrive(16);
//I'm pretty sure i'm old enough to drive, why is this code making me use a bike??

//An object is something we can store information in.  Like a well organized drawer with labels.
//If you want to access a value you call it out by the label you've given it.
let mySpecialObject = {
    name: "so special",
    color: "grey",
    size: "huge!"
}
let value = mySpecialObject.color;
//value:

value = mySpecialObject.size;
//value:

value = mySpecialObject.name;
//value:

mySpecialObjen.size = "small";
value = mySpecialObject.size;
//value:

mySpecialObject.setName = function(yourName){
    mySpecialObject.name = yourName;
}
mySpecialObject.setName("bob");
value = mySpecialObject.name;
//value:

if(mySpecialObject.name === 'bob'){
    value = true;
}
//value:

value = mySpecialObject.color === 'grey';
//value:'