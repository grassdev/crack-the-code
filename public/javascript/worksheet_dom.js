//where did the javascript programming languange come from?
//___________________________________________________________

//how would we get access to a web page?  We have two major objects, the 'window' object
//and the 'document' object.
//what do you thing the 'window' object is about?
//___________________________________________________________

//what do you thing the 'document' object is about?
//___________________________________________________________

//HINT!! You may need to open up your developer tools to figure this one out!
//Open up chrome and hold down ctrl+shift+I, or f12, or ask for help!
document.title = "I'm now set to this!";
//what changes?______________________________________________

window.alert('howdy!');
//what happenes?_____________________________________________

//so if these are objects we can treat them like objects too
document.whateverLabelIWant = true;
if(document.whateverLabelIWant){
    document.title = "Oh yeah!";
}
//Does anything change? If so, what?__________________________

//since you have the dev tools up let's experiement with that wierd equals again
let result = 1 == ['1'];
//result?_________________________________

result = 1 === ['1']
//result?_________________________________
//what does that tell you about using '==' ?___________________

//now back to the 'document' object.  We can add pieces of our website directly to
//the 'DOM' using functions that the 'document' object has.
let divTag = document.createElement('h1');
divTag.innerText = "Maybe this could totally add some text to our screen!";
let body = document.querySelector('body');
body.appendChild(divTag);
//Do you think this would change the website?  TRUE / FALSE
//Would this create an image of a rabbit dancing?  TRUE / FALSE
//would this add the value assigned to the innerText property to the screen?  TRUE / FALSE

//write a function that adds a string to the DOM: