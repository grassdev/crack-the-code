const moment = require('moment');
class Person {
    constructor(height, weight, eyeColor, birthDate){
        this.height = height;
        this.weight = weight;
        this.eyeColor = eyeColor;
        this.birthDate(birthDate);
    }

    /**
     * @param {string | number | Date} birthDate
     */
    birthDate (birthDate){
        if (birthDate instanceof Date){
            this.birthDay = birthDate;
        } else {
            try {
                this.birthDay = new Date(birthDate);
            } catch(err) {
                console.log(`hey, you didn't give me something i could turn into a date.  Try again.`);
            }
        }
    }

    age(){
        return moment().diff(this.birthDay, 'year');
    }
}
let me = new Person(`5'10`, 200, 'brown', new Date('09/18/1981'));
console.log(me.age());