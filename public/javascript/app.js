//these are variables that reference parts of the html part of the website.
//they tie into our "view"
let theView = document.getElementById('display');
let theDialogueSection = document.getElementsByName('dialogues')[0];
let answers = document.getElementsByName("answer")[0];

//Here are our dialogue questions.  We are setting each of our questions up as variables so we can use them
//multiple times without having to right the question out over and over.
let crocodileQuestionOne = "Do I have sharp teeth?";
let crocodileQuestionTwo = "Do I have green skin?";
let crocodileQuestionThree = "Do I swim?";
let birdQuestionOne = "Do I fly";
let birdQuestionTwo = "Do I have feathers?";
let birdQuestionThree = "Do I have scales?";

//This variable is the question we are currently on, so when the user click yes or no this is the question they are answering.
//We will be swapping the value of this variable around a lot!
let currentDialogue = crocodileQuestionOne;

//This is called a function, but we will talk more about that later.  What it's doing is adding our current question message to the screen.
addQuestion(currentDialogue);

//this function is handling the YES button
function yesEventHandler(event, stuff){
    if(currentDialogue === crocodileQuestionOne){
        currentDialogue = crocodileQuestionTwo;
        addQuestion(currentDialogue);
    } else if(currentDialogue === crocodileQuestionTwo){
        currentDialogue = crocodileQuestionThree;
        addQuestion(currentDialogue);
    } else if(currentDialogue === crocodileQuestionThree) {
        addQuestion("I'm a crocodile!");
        addImage('crocodile.png');
        removeButtons();
    } else if(currentDialogue === birdQuestionOne){
        currentDialogue = birdQuestionTwo;
        addQuestion(currentDialogue);
    } else if(currentDialogue === birdQuestionTwo){
        currentDialogue = birdQuestionThree;
        addQuestion(currentDialogue);
    } else if(currentDialogue === birdQuestionThree){
        addQuestion("Oh... I don't know what I am then.");
        addImage('sadFace.png');
        removeButtons();
    }
}

//this function is handling the NO button
function noEventHandler(){
    if(currentDialogue === crocodileQuestionOne){
        currentDialogue = birdQuestionOne;
        addQuestion(currentDialogue);
    } else if(currentDialogue === crocodileQuestionTwo){
        currentDialogue = birdQuestionOne;
        addQuestion(currentDialogue);
    } else if(currentDialogue === crocodileQuestionThree){
        currentDialogue = birdQuestionOne;
        addQuestion(currentDialogue);
    } else if(currentDialogue === birdQuestionThree){
        addQuestion("I'm a bird!");
        addImage('bird.png');
        removeButtons();
    } else {
        addQuestion("Oh... I don't know what I am then.");
        addImage('sadFace.png');
        removeButtons();
    }
}

//This function is adding a message to the dialogue section of the screen.
function addQuestion(someText){
    let paragraph = document.createElement("p");
    paragraph.innerText = someText;
    addAnElementToDialogue(paragraph);
}

//Add a picture!
function addImage(imageName){
    let img = document.createElement('img');
    img.setAttribute('src', 'resources/' + imageName);
    addAnElementToDialogue(img);
}

//helper function to add things to the dialogue section of the screen
function addAnElementToDialogue(element){
    let div = document.createElement('div');
    div.setAttribute('class', 'dialogue');
    div.append(element);
    theDialogueSection.append(div);
 }

 //remove buttons
 function removeButtons(){
    answers.innerHTML = '';
 }